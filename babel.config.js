module.exports = {
  presets: [
    '@vue/app'
  ],
  babelrcRoots: [
    '.',
    'packages/*',
    'utils/*'
  ]
}
