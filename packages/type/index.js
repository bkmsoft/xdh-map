import Type from './src/type'

Type.install = function (Vue) {
  Vue.component(Type.name, Type)
}
export default Type
